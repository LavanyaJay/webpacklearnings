import Image from './image.jpg';
const addImage = () => {
  const img = document.createElement('img');
  img.alt = 'Mount Pilatus';
  img.width = '300';
  img.src = Image;
  const body = document.querySelector('body');
  body.appendChild(img);
};
export default addImage;
