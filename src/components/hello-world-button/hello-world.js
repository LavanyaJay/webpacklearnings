import './hello-world.css';
class HelloWorldButton {
  render() {
    const button = document.createElement('button');
    const body = document.querySelector('body');
    button.innerHTML = 'Hello World';
    button.classList.add('hello-world-button');
    button.onclick = () => {
      const p = document.createElement('p');
      p.classList.add('hello-world-text');
      p.innerHTML = 'Hi Lavs';
      body.appendChild(p);
    };

    body.appendChild(button);
  }
}

export default HelloWorldButton;
